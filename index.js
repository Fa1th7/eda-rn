import React from 'react';
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { AuthProvider } from './src/contexts/AuthContext';
import { AxiosProvider } from './src/contexts/AxiosContext';


const Root = () => {
    return (
      <AuthProvider>
        <AxiosProvider>
          <App />
        </AxiosProvider>
      </AuthProvider>
    );
  };

AppRegistry.registerComponent(appName, () => Root);
