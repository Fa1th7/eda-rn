import {LogBox} from 'react-native';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

import {AppNavigator} from './src/app';
import {configureDesignSystem} from './src/utils/designSystem';
import {hydrateStores, StoresProvider, useStores} from './src/stores';
import {initServices, ServicesProvider} from './src/services';
import * as Keychain from 'react-native-keychain';
import { AuthContext } from './src/contexts/AuthContext';
import clearStorage from './src/utils/clearMMKV';
import Geocoder from 'react-native-geocoding';


LogBox.ignoreLogs([
  'EventEmitter.removeListener',
  '`new NativeEventEmitter()`',
  '[react-native-gesture-handler] Seems like', // https://github.com/software-mansion/react-native-gesture-handler/issues/1831
]);
type valueType = {password: string};
export default (): JSX.Element => {
  const authContext = useContext(AuthContext);
  const [ready, setReady] = useState(false);
  const {auth, user} = useStores()
  const loadJWT = useCallback(async () => {
    try {
      const value = await Keychain.getGenericPassword();
      const jwt = JSON.parse(value.password);
      auth.login({accessToken:jwt.accessToken})
      console.log(jwt.accessToken)
      user.getUser();
    } catch (error) {
     // console.log(`Keychain Error: ${error.message}`);
     auth.login({accessToken:""})
    }
  }, []);

  const startApp = useCallback(async () => {
    // clearStorage('AuthStore.name');
    await hydrateStores();
    await initServices();
    configureDesignSystem();
    loadJWT();
    setReady(true);
    Geocoder.init("AIzaSyAXlwW_5H5LGJUY-aAyuBO8zh9i0xE2ECQ", {language : "en"})
    
    // auth.clearStoredDate()
    
  }, []);

  useEffect(() => {
    startApp();
  }, [startApp]);

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <StoresProvider>
        <ServicesProvider>{ready ? <AppNavigator /> : null}</ServicesProvider>
      </StoresProvider>
    </GestureHandlerRootView>
  );
};
