import React from 'react'
import { View, Colors } from 'react-native-ui-lib';
import { ActivityIndicator } from 'react-native';

export default function Loader() {
  return (
    <View style = {{flex:1, justifyContent:"center", alignItems:"center"}}>
        <ActivityIndicator size={'large'} color = {Colors.whitish}/>
    </View>
  )
}
