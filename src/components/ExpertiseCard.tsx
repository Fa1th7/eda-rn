import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { responsiveWidth } from 'react-native-responsive-dimensions';
import { Text, View, Colors } from 'react-native-ui-lib';
import getRandomColor from '../utils/getRandomColor';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';

type propCard = {
    boardName: string,
    subjectName: string,
    index: number,
    onPressDelete: () => void
}
export const ExpertiseCard = (props: propCard) => {
    return (
        <View>
            <LinearGradient useAngle angle={-180} colors={[Colors.primary, "#ba03fc", "#fc03f0"]} style={{ ...styles.container, }}>
                <Text style={styles.subjectNameText}>{props.subjectName}</Text>
                <Text style={styles.boardNameText}>{props.boardName.split('-')[0]} </Text>
                <Text style={styles.boardNameSubText}>{props.boardName.split('-')[1] ? props.boardName.split('-')[1] : ""}</Text>
            </LinearGradient>
            <TouchableOpacity onPress={() => props.onPressDelete()} style={styles.deleteContainer}>
                <MaterialIcons name={'delete'} size={24} color={Colors.$iconDanger} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: "80%",
        width: responsiveWidth(70),
        marginHorizontal: 16,
        borderRadius: 16,
        padding: 16,
        // backgroundColor: Colors.primary
    },
    boardNameText: {
        fontSize: 16,
        color: '#ccc',
        fontFamily: "Montserrat-Medium",
        marginTop: 8
    },
    subjectNameText: {
        fontSize: 18,
        color: '#ccc',
        fontFamily: "Montserrat-SemiBold",
    },
    boardNameSubText: {
        fontSize: 14,
        color: '#ccc',
        fontFamily: "Montserrat-Regular",
        marginTop: 8
    },
    deleteContainer: {
        position: "absolute",
        right: 8,
        top: -8,
        backgroundColor: Colors.white,
        width: 28,
        height: 28,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 14,
    }
})
