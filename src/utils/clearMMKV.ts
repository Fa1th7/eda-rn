import {MMKV} from 'react-native-mmkv';

const storage = new MMKV();
const clearStorage = async (key:string) => {
    storage.delete(key);
}

export default clearStorage;