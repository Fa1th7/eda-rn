import axios from 'axios';
import { stores } from '../stores';



const API = axios.create({
  
  baseURL: `http://192.168.0.137:8000/device`,
  timeout: 100000,
  'Content-Type': 'application/x-www-form-urlencoded',
});

API.interceptors.request.use(
  async config => {
    const token = `Bearer ${stores.auth.accessToken}`
    // console.log(token);
    if (token) {
      config.headers.Authorization = token;
    } 
    // else {
    //   delete API.defaults.headers.common.Authorization;
    // }
    // console.log(config.url);
    return config;
  },

  error => {
    Promise.reject(error)
  },
);
// API.interceptors.request.use(config => {
//   if (config.delayed) {
//     return new Promise(resolve => setTimeout(() => resolve(config), 600));
//   }
//   return config;
// });
// API.interceptors.response.use(undefined, function retry(err) {
//   if (
//     err.response &&
//     (err.response.status === 403 || err.response.status === 401)
//   ) {
//     replace('phonenumber');
//   }
//   return Promise.reject(err);
// });

export default API;
