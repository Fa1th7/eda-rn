import Api from './Api';
import axios from 'axios';
import {Buffer} from 'buffer';
var RNFS = require('react-native-fs');
const fileUpload = ({isPublic, path, onUpload, onUploadProgress}) => {
  let outcome = null;

  const onChange = async file => {
    const base64 = await RNFS.readFile(file.path, 'base64');
    const buffer = Buffer.from(base64, 'base64');
    if (file) {
      let data = {
        name: file.name,
        type: file.type,
        public: isPublic,
        path,
      };
      console.log(data, 'data');
      await Api.post('api/v1/file/upload', data).then(async result => {
        var options = {
          headers: {
            'Content-Type': data.type,
          },
          onUploadProgress: function (progressEvent) {
            var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
            if (onUploadProgress) {
              onUploadProgress(percentCompleted,"kkkkkk");
            }
          },
        };

        if (result.data.data.ContentDisposition === 'attachment') {
          options['headers']['Content-Disposition'] = 'attachment';
        }
        console.log((result.data.data), 'result.data.data.url');

        axios
          .put(result.data.data.signedRequest, buffer, options)
          .then(() => {
            outcome = null;
            let response = {
              path: result.data.data.path,
              type: file.type,
              url: result.data.data.url,
              size: file.size,
              name: file.name,
              public: isPublic,
            };
            onUpload(response);
          })
          .catch(error => {
            console.log((error), 'error');
          });
      });
    }
  };
  return {onChange};
};

export default fileUpload;
