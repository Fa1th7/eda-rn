const getRandomColor = (index: number) => {
    let maxVal = 0xFFFFFF; // 16777215
    let randomNumber = index * maxVal; 
    randomNumber = Math.floor(randomNumber);
    let randomNumberStr = randomNumber.toString(16);
    let randColor = randomNumberStr.padStart(6, "");   
    return `#${randColor.toUpperCase()}`
}

export default getRandomColor;