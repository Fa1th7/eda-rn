import React, { FC, useRef, useState, useEffect, useCallback } from 'react'
import { Text, TextField, TouchableOpacity, View, Colors } from 'react-native-ui-lib';
import { observer } from 'mobx-react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { StyleSheet, Platform, Dimensions, Linking, Alert, PermissionsAndroid, ToastAndroid } from 'react-native';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { AutocompleteDropdown, AutocompleteDropdownRef } from 'react-native-autocomplete-dropdown';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import LinearGradient from 'react-native-linear-gradient';
import { useStores } from '../../stores/index';


type addressType = {
    id: string,
    title: string,
}
type addressDataType = {
    id: string,
    title: string,
    latitude: number,
    longitude: number,
}
export const TeacherLocationUpdate: FC = observer(() => {
    const boardDropdownController = useRef<AutocompleteDropdownRef>(null);
    const [addressList, setAddressList] = useState<addressType[]>([]);
    const [addressListData, setAddressListData] = useState<addressDataType[]>([]);
    const [currentAddress, setCurrentAddress] = useState<addressType | null>(null)
    const [cuurentAddresData, setCurrentAddresData] = useState<addressDataType | null>(null)
    const [location, setLocation] = useState<{ latitude: number, longitude: number, }>({
        latitude: 37.78825,
        longitude: -122.4324,
    });
    const [region, setRegion] = useState<{ latitude: number, longitude: number, latitudeDelta: number, longitudeDelta: number }>({
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
    });
    const { teacher } = useStores();
    

    const getCurrentLocationAddress = useCallback(async (q: string) => {
       
        Geocoder.from(q)
		.then(json => {
			let locations = json.results.map(result => {
                return {
                    id: result.place_id,
                    title: result.formatted_address,
                }
    
            });
            let locationData = json.results.map(result => {
                return {
                    id: result.place_id,
                    title: result.formatted_address,
                    latitude: result.geometry.location.lat,
                    longitude: result.geometry.location.lng,
                }
    
            })
            setAddressList(locations);
            setAddressListData(locationData);
		})
		.catch(error => console.warn(error));
        // console.log(resp.data,"jjj")
        // if (resp.data?.data && resp.data.data.length > 0) {
        //     let temp = resp.data.data.map((item: { _id: string, name: string, mediumData: { name: string }, longName: string }, index: number) => {
        //         return {
        //             title: `${item.name}(${item.mediumData.name}) ${item.longName ? '-' : ''}${item.longName ? item.longName : ''}`,
        //             id: item._id,

        //         }
        //     })
        //     setBoardList(temp);
        // }
        // else {
        //     setBoardList([])
        // }
    }, [location])
    const hasPermissionIOS = async () => {
        const openSetting = () => {
            Linking.openSettings().catch(() => {
                Alert.alert('Unable to open settings');
            });
        };
        const status = await Geolocation.requestAuthorization('whenInUse');

        if (status === 'granted') {
            return true;
        }

        if (status === 'denied') {
            Alert.alert('Location permission denied');
        }

        if (status === 'disabled') {
            Alert.alert(
                `Turn on Location Services to allow EDA to determine your location.`,
                '',
                [
                    { text: 'Go to Settings', onPress: openSetting },
                    { text: "Don't Use Location", onPress: () => { } },
                ],
            );
        }

        return false;
    };

    const hasLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            const hasPermission = await hasPermissionIOS();
            return hasPermission;
        }

        if (Platform.OS === 'android' && Platform.Version < 23) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show(
                'Location permission denied by user.',
                ToastAndroid.LONG,
            );
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show(
                'Location permission revoked by user.',
                ToastAndroid.LONG,
            );
        }

        return false;
    };
    const getLocation = async () => {
        const hasPermission = await hasLocationPermission();

        if (!hasPermission) {
            return;
        }

        Geolocation.getCurrentPosition(
            (position) => {
                setLocation({ latitude: position.coords.latitude, longitude: position.coords.longitude });
                Geocoder.from({ latitude: position.coords.latitude, longitude: position.coords.longitude }).then(json => {
                    // let locations = json.results.map(result => {
                    //     return {
                    //         id: result.place_id,
                    //         title: result.formatted_address,
                    //     }
                    // });
                    // let locationData = json.results.map(result => {
                    //     return {
                    //         id: result.place_id,
                    //         title: result.formatted_address,
                    //         latitude: result.geometry.location.lat,
                    //         longitude: result.geometry.location.lng,
                    //     }
                    // })
                    // setAddressList(locations);
                    // setAddressListData(locationData);
                    console.log(json.results,"jjj")
                }).catch(error => console.warn(error));
                console.log(position);
            },
            (error) => {
                Alert.alert(`Code ${error.code}`, error.message);
                // setLocation(null);
                console.log(error);
            },
            {
                accuracy: {
                    android: 'high',
                    ios: 'best',
                },
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
                distanceFilter: 0,
                forceRequestLocation: true,
            },
        );
    };
    useEffect(() => {
        getLocation()
    }, [])

    useEffect(() => {
        setRegion({ ...region, latitude: location.latitude, longitude: location.longitude })
    }, [location])

    const onSaveLocation =  () => {
        teacher.updateTeacherLocation(teacher._id, {latitude: location.latitude, longitude: location.longitude},4)
    }
    return (


        <View style={styles.container}>
            <MapView
                onPress={(ev) => setLocation(ev.nativeEvent.coordinate)}
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                region={region}
            // onRegionChangeComplete = {(reg: { latitude: number, longitude: number, latitudeDelta: number, longitudeDelta: number }) => {
            //     setRegion(reg)
            // }}

            >
                <Marker coordinate={location}>

                </Marker>
            </MapView>
            <View style={{ position: "absolute", top: 100, width: "100%" }}>
                <AutocompleteDropdown
                    // ref={boardRef}
                    controller={controller => {
                        boardDropdownController.current = controller
                    }}
                    // initialValue={'1'}
                    direction={Platform.select({ ios: 'down' })}
                    dataSet={addressList}
                    onChangeText={(txt) => getCurrentLocationAddress(txt)}
                    onSelectItem={(item) => {
                        if (item && item.title && item.id) {
                            setCurrentAddress({ title: item.title, id: item.id });
                        }

                    }}
                    debounce={600}
                    suggestionsListMaxHeight={Dimensions.get('window').height * 0.4}
                    // onClear={onClearBoardPress}
                    //  onSubmit={(e) => onSubmitSearch(e.nativeEvent.text)}
                    // onOpenSuggestionsList={geAddressList}
                    // loading={boardLoading}
                    useFilter={false} // set false to prevent rerender twice
                    textInputProps={{
                        placeholder: 'Start typing to search',
                        autoCorrect: false,
                        autoCapitalize: 'none',
                        style: {
                            borderRadius: 25,
                            backgroundColor: '#383b42',
                            color: '#fff',
                            paddingLeft: 18,
                        },
                    }}
                    rightButtonsContainerStyle={{
                        right: 8,
                        height: 30,

                        alignSelf: 'center',
                    }}
                    inputContainerStyle={{
                        backgroundColor: '#383b42',
                        // marginHorizontal:16
                        // borderRadius: 25,
                    }}
                    suggestionsListContainerStyle={{
                        backgroundColor: '#383b42',
                        // marginHorizontal:16
                    }}
                    containerStyle={{ height: 52, justifyContent: "center", alignItems: "center", marginHorizontal: 16, marginTop: 16 }}
                    renderItem={(item, text) => <Text key={item.id} style={{ color: '#fff', padding: 15 }}>{item.title}</Text>}
                    //   ChevronIconComponent={<Feather name="chevron-down" size={20} color="#fff" />}
                    //   ClearIconComponent={<Feather name="x-circle" size={18} color="#fff" />}
                    inputHeight={50}
                    showChevron={false}
                    closeOnBlur={false}
                />
            </View>
            <TouchableOpacity onPress={() => onSaveLocation()} style = {{position:"absolute", backgroundColor:'#383b42', bottom:50, height:60, width:responsiveWidth(80), alignSelf:"center", justifyContent:"center", alignItems:"center", borderRadius:30 }}>
               
                <Text style = {{color:Colors.white, fontSize:24, fontFamily:"Montserrat-SemiBold"}}>Save location</Text>
               
            </TouchableOpacity>
        </View>
    )

})

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

