import React, { FC, useCallback, useEffect, useRef, useState } from 'react'
import { Picker, PickerModes, Text, Colors, Incubator, Button, View } from 'react-native-ui-lib';
import { observer } from 'mobx-react';
import { SafeAreaView, ScrollView, StyleSheet, Platform, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native';
import API from '../../utils/Api';
import { AutocompleteDropdown, AutocompleteDropdownRef } from 'react-native-autocomplete-dropdown';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { ExpertiseCard } from '../../components/ExpertiseCard';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';
import { useStores } from '../../stores';
import { NavService } from '../../services/navigation/index';
const options = [
    { label: 'JavaScript', value: 'js' },
    { label: 'Java', value: 'javakk' },
    { label: 'Python', value: 'python' },
    { label: 'C++', value: 'c++', disabled: true },
    { label: 'Perl', value: 'perl' }
];
type boardType = {
    title: string;
    id: string;
};
type subjectType = {
    title: string;
    id: string;
}
type expertiseType = {
    subject: string,
    subjectId: string,
    board: string,
    boardId: string,
}

export const TeacherExpertiseUpdate: FC = observer(() => {
    const [boardData, setBoadData] = React.useState<boardType | null>(null);
    const [subjectData, setSubjectData] = React.useState<subjectType | null>(null);
    const [boardList, setBoardList] = React.useState<boardType[]>([]);
    const [subjectList, setSubjectList] = React.useState<subjectType[]>([]);
    const boardRef = useRef(null);
    const subjectRef = useRef(null);
    const boardDropdownController = useRef<AutocompleteDropdownRef>(null);
    const subjectDropdownController = useRef<AutocompleteDropdownRef>(null);
    const [boardLoading, setBoardLoading] = useState<boolean>(false);
    const [subjectLoading, setSubjectLoading] = useState<boolean>(false);
    const [totalExpertise, setTotalExpertis] = useState<expertiseType[]>([]);
    const cardScrollRef = useRef<ScrollView>(null)
    const {teacher} = useStores()

    const getBoardList = useCallback(async (q: string) => {
        let resp = await API.post('/api/v1/board/list', {
            options: { name: q }
        }
        );
        // console.log(resp.data,"jjj")
        if (resp.data?.data && resp.data.data.length > 0) {
            let temp = resp.data.data.map((item: { _id: string, name: string, mediumData: { name: string }, longName: string }, index: number) => {
                return {
                    title: `${item.name}(${item.mediumData.name}) ${item.longName ? '-' : ''}${item.longName ? item.longName : ''}`,
                    id: item._id,

                }
            })
            setBoardList(temp);
        }
        else {
            setBoardList([])
        }
    }, [])

    const onClearBoardPress = useCallback(() => {
        setBoardList([]);
        setBoadData(null);
    }, [])

    const onOpenBoardList = useCallback((isOpened: boolean) => { }, [])
    const getSubjectList = useCallback(async (q: string) => {
        let resp = await API.post('/api/v1/subject/list', {
            options: {
                board: boardData?.id,
                name: q
            }
        })
        console.log(resp.data, "jjj")
        if (resp.data?.data && resp.data.data.length > 0) {
            let temp = resp.data.data.map((item: { id: string, name: string }, index: number) => {
                return {
                    title: item.name,
                    id: item.id,
                }
            })
            setSubjectList(temp);
        }
        else {
            setSubjectList([])
        }
    }, [boardData])

    const onClearSubjectPress = useCallback(() => {
        setSubjectList([]);
        setSubjectData(null);
    }, [])

    const onAddExpertisHandler = () => {
        if (subjectData && boardData) {
            if (totalExpertise.find(item => item.subjectId === subjectData.id && item.boardId === boardData.id)) {
                Snackbar.show({
                    text: 'Already added this expertise',
                    textColor: '#fc03ca',
                    backgroundColor: '#383b42',
                    duration: Snackbar.LENGTH_LONG,
                    action: {
                        text: 'Clear',
                        onPress: () => {

                            boardDropdownController?.current?.clear();
                            // subjectDropdownController?.current?.clear();
                            setBoadData(null);
                            setSubjectData(null);
                            onClearSubjectPress()
                            onClearBoardPress();
                        }
                    }
                })
            }
            else {
                let temp = totalExpertise;
                temp.push({
                    subject: subjectData.title,
                    subjectId: subjectData.id,
                    board: boardData.title,
                    boardId: boardData.id,
                })
                setTotalExpertis(temp);
                setBoadData(null);
                setSubjectData(null);
                onClearSubjectPress()
                onClearBoardPress();
                setTimeout(() => {
                    if (cardScrollRef.current) cardScrollRef.current.scrollToEnd({ animated: true });
                }, 400);

                boardDropdownController?.current?.clear();
                subjectDropdownController?.current?.clear();
            }


        }
    }

    const onOpenSubjectList = useCallback((isOpened: boolean) => { }, [])
    // console.log(totalExpertise, boardData, subjectData, "totalExpertise")
    // console.log(boardList, "jkk");
    const onRemoveExpertise = useCallback((ind: number) => {
        setTotalExpertis(totalExpertise.filter((item, index) => index !== ind))
    }, [])
    const onPressNextHandler = () => {
        if(totalExpertise.length > 0){
            let tempBoardList = totalExpertise.map(item => item.boardId)
            let tempSubjectList = totalExpertise.map(item => item.subjectId)
            teacher.updateTeacher(teacher._id, {expertise: tempSubjectList, board: tempBoardList, onBoardStep:2, afterUpdate: () => {
               console.log("updated")
            }})
        }
          
    }
    return (
        <SafeAreaView style={styles.container}>

            {totalExpertise.length > 0 ? <ScrollView ref={cardScrollRef} showsHorizontalScrollIndicator={false} horizontal style={styles.scrollContainer} contentContainerStyle={{
                alignItems: "center",
                justifyContent: "center",
            }}>
                {
                    totalExpertise.map((item: expertiseType, index: number) => {
                        return (
                            <ExpertiseCard key={index} onPressDelete={() => onRemoveExpertise(index)} index={index} boardName={item.board} subjectName={item.subject} />
                        )
                    })
                }
            </ScrollView> : null}
            <View style={styles.autoContainer}>
                <AutocompleteDropdown
                    // ref={boardRef}
                    controller={controller => {
                        boardDropdownController.current = controller
                    }}
                    // initialValue={'1'}
                    direction={Platform.select({ ios: 'down' })}
                    dataSet={boardList}
                    onChangeText={getBoardList}
                    onSelectItem={(item) => {
                        if (item && item.title && item.id) {
                            setBoadData({ title: item.title, id: item.id });
                        }

                    }}
                    debounce={600}
                    suggestionsListMaxHeight={Dimensions.get('window').height * 0.4}
                    onClear={onClearBoardPress}
                    //  onSubmit={(e) => onSubmitSearch(e.nativeEvent.text)}
                    onOpenSuggestionsList={onOpenBoardList}
                    loading={boardLoading}
                    useFilter={false} // set false to prevent rerender twice
                    textInputProps={{
                        placeholder: 'Search Board (Eg: wbbse)',
                        autoCorrect: false,
                        autoCapitalize: 'none',
                        style: {
                            borderRadius: 25,
                            backgroundColor: '#383b42',
                            color: '#fff',
                            paddingLeft: 18,
                        },
                    }}
                    rightButtonsContainerStyle={{
                        right: 8,
                        height: 30,

                        alignSelf: 'center',
                    }}
                    inputContainerStyle={{
                        backgroundColor: '#383b42',
                        // marginHorizontal:16
                        // borderRadius: 25,
                    }}
                    suggestionsListContainerStyle={{
                        backgroundColor: '#383b42',
                        // marginHorizontal:16
                    }}
                    containerStyle={{ height: 52, justifyContent: "center", alignItems: "center", marginHorizontal: 16, marginTop: 16 }}
                    renderItem={(item, text) => <Text key={item.id} style={{ color: '#fff', padding: 15 }}>{item.title}</Text>}
                    //   ChevronIconComponent={<Feather name="chevron-down" size={20} color="#fff" />}
                    //   ClearIconComponent={<Feather name="x-circle" size={18} color="#fff" />}
                    inputHeight={50}
                    showChevron={false}
                    closeOnBlur={false}
                />
                {boardData ? <AutocompleteDropdown
                    // ref={subjectRef}
                    controller={controller => {

                        subjectDropdownController.current = controller
                    }}
                    // initialValue={'1'}
                    direction={Platform.select({ ios: 'down' })}
                    dataSet={subjectList}
                    onChangeText={getSubjectList}
                    onSelectItem={(item) => {
                        if (item && item.title && item.id) {
                            setSubjectData({ title: item.title, id: item.id });
                        }

                    }}
                    debounce={600}
                    suggestionsListMaxHeight={Dimensions.get('window').height * 0.4}
                    onClear={onClearSubjectPress}
                    //  onSubmit={(e) => onSubmitSearch(e.nativeEvent.text)}
                    onOpenSuggestionsList={onOpenSubjectList}
                    loading={subjectLoading}
                    useFilter={false} // set false to prevent rerender twice
                    textInputProps={{
                        placeholder: 'Search Subject (Eg: wbbse)',
                        autoCorrect: false,
                        autoCapitalize: 'none',
                        style: {
                            borderRadius: 25,
                            backgroundColor: '#383b42',
                            color: '#fff',
                            paddingLeft: 18,
                        },
                    }}
                    rightButtonsContainerStyle={{
                        right: 8,
                        height: 30,

                        alignSelf: 'center',
                    }}
                    inputContainerStyle={{
                        backgroundColor: '#383b42',
                        // borderRadius: 25,
                    }}
                    suggestionsListContainerStyle={{
                        backgroundColor: '#383b42',
                    }}
                    containerStyle={{ height: 52, justifyContent: "center", alignItems: "center", marginTop: 16, marginHorizontal: 16 }}
                    renderItem={(item, text) => <Text key={item.title} style={{ color: '#fff', padding: 15 }}>{item.title}</Text>}
                    //   ChevronIconComponent={<Feather name="chevron-down" size={20} color="#fff" />}
                    //   ClearIconComponent={<Feather name="x-circle" size={18} color="#fff" />}
                    inputHeight={50}
                    showChevron={false}
                    closeOnBlur={false}
                /> : null}

                <Button onPress={() => onAddExpertisHandler()} disabled={!subjectData && !boardData} style={styles.buttonStyle} labelStyle={styles.buttonTextStyle} label='Add expertise' size={Button.sizes.large} backgroundColor={Colors.primary} />
                {totalExpertise.length>0?<LinearGradient colors={["#fc03f8", Colors.primary,]} style={styles.nextButton}>
                  {!teacher.isUpdaing?  <TouchableOpacity style={{
                        width: "100%", height: "100%", alignItems: "center",
                        justifyContent: "center",
                    }}
                    onPress = {onPressNextHandler}
                    >
                        <Text style={styles.nextTextStyle}>Next</Text>
                    </TouchableOpacity>:<ActivityIndicator size={'large'} color = {Colors.white} />}
                </LinearGradient>: null}
            </View>
        </SafeAreaView>
    )
})

const styles = StyleSheet.create({
    container: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        padding: 24,
    },

    scrollContainer: {
        height: "100%",
        width: "100%",


    },
    buttonStyle: {
        marginTop: 48,
        borderRadius: 36,
        marginHorizontal: 16,
        height: 52,
        width: responsiveWidth(60),
        alignSelf: "center"
    },
    buttonTextStyle: {
        color: "#fff",
    },
    autoContainer: {
        height: "70%",
    },
    nextButton: {
        height: 72,
        width: "100%",
        alignSelf: "flex-end",
        position: "absolute",
        bottom: 0,
        borderRadius: 16,
        alignItems:"center",
        justifyContent:"center"


    },
    nextTextStyle: {
        color: "#fff",
        fontSize: 24,
        fontFamily: "Montserrat-SemiBold",
    }
})
