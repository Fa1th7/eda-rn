import { observer } from 'mobx-react'
import React, { FC, useEffect } from 'react'
import { Text, View, Colors, ActionSheet, Button } from 'react-native-ui-lib';
import { SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import { useStores } from '../../stores/index';
import ImagePicker from 'react-native-image-crop-picker';
import fileUpload from '../../utils/fileupload';
import FastImage from 'react-native-fast-image';

type profilePicDataType = {
    url: string,
    name: string,
    path: string,
    public: boolean,
    size: number,
    type: string,

} | null;
const TeacherUpdatePicture: FC = observer(() => {
    const { teacher } = useStores();
    const [isPickeOptionVisible, setIsPickerOptionVisible] = React.useState(false);
    const [profilePicData, setProfilePicData] = React.useState<profilePicDataType>(null);

    useEffect(() => {
        if(teacher.profilePic){
            setProfilePicData(teacher.profilePic);
        }
    },[teacher.profilePic])

   const updateHandler = () => {
    if(profilePicData) {
        teacher.updateTeacherProfilePic(
            teacher._id,
            profilePicData,
            3
        )
    }
    
   }
   

    const onOpenSelection = async (params: string) => {

        if (params === 'camera') {
            await ImagePicker.openCamera({
                width: 300,
                height: 400,
                cropping: true
            }).then(image => {
                console.log(image);
            });
        }
        else {
            await ImagePicker.openPicker({
                width: 300,
                height: 400,
                cropping: true
            }).then(image => {
                fileUpload({
                    isPublic: true,
                    path: "profilepic",
                    onUpload: (data: profilePicDataType) => {
                        setProfilePicData(data);
                    },
                    onUploadProgress: (data: string) => {
                        console.log(data)
                    }
                }).onChange({

                    file: image.path,
                    name: image.filename,
                    type: image.mime,
                    path: image.path,
                    size: image.size
                })
            });
        }


    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.cardContainer}>
                <View style={styles.bannerConatiner}>
                    <LinearGradient colors={["#ba03fc", "#fc03f0"]} style={{ width: "100%", height: "100%", borderRadius: 16, }}>

                    </LinearGradient>
                    <View style={styles.avatarConatiner}>
                        {profilePicData?.url ? <FastImage
                            source={{ uri: profilePicData.url }}
                            style={styles.avatar}
                            resizeMode={FastImage.resizeMode.cover}
                        /> : <Feather name={'user'} size={80} color={'white'} />}
                        <TouchableOpacity onPress={() => onOpenSelection('gallery')} style={styles.editProfileContainer}>
                            <MaterialCommunityIcons name={'account-edit-outline'} size={32} color={Colors.$iconDanger} />
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={{ marginTop: 70, paddingHorizontal: 16 }}>
                    <Text style={styles.nameText}>{teacher.name}</Text>
                    {teacher.email ? <Text style={styles.emailText}>Email: <Text >{teacher.email}</Text></Text> : null}
                    {teacher.mobileNo ? <Text style={styles.mobileText}>Phone: <Text >{teacher.mobileNo}</Text></Text> : null}

                </View>

            </View>
            <TouchableOpacity onPress={updateHandler} style={{ height: 56, width: "80%", marginTop: 24, borderRadius:36 }}>
                <LinearGradient colors={["#fc03f8", Colors.primary,]} style={{ width: "100%", height: "100%", alignItems: "center", justifyContent: "center",  borderRadius:36 }}>
                    <Text style={{
                        color: "#fff",
                        fontSize: 24,
                        fontFamily: "Montserrat-SemiBold",
                    }}>Update</Text>
                </LinearGradient>
            </TouchableOpacity>
            <ActionSheet
                visible={isPickeOptionVisible}
                title={'Upload your profile picture'}
                message={'You can choose between these options'}
                cancelButtonIndex={3}
                // onDismiss={() => setIsPickerOptionVisible(false)}
                // onModalDismissed={() => setIsPickerOptionVisible(false)}
                // destructiveButtonIndex={0}
                options={[
                    {
                        label: 'Select from photos', onPress: () => {
                            setIsPickerOptionVisible(false);
                            onOpenSelection('gallery')
                        }
                    },
                    {
                        label: 'Capture from your camera', onPress: () => {
                            setIsPickerOptionVisible(false);
                            onOpenSelection('camera')
                        }
                    },
                    { label: 'Cancel', onPress: () => setIsPickerOptionVisible(false) },
                ]}
            />
        </SafeAreaView>
    )
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#000000"
    },
    cardContainer: {
        width: "80%",
        height: "60%",
        backgroundColor: "#4d4f52",
        borderRadius: 16,
        shadowColor: "white",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.17,
        shadowRadius: 2.54,
        elevation: 3
    },
    bannerConatiner: {
        width: "100%",
        height: "40%",
        borderRadius: 16,
    },
    avatarConatiner: {
        height: 140,
        width: 140,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 160,
        borderWidth: 1,
        borderColor: Colors.white,
        position: "absolute",
        bottom: -70,
        backgroundColor: "#4d4f52"

    },
    editProfileContainer: {
        position: "absolute",
        right: -12,
        bottom: 20,
        width: 42,
        height: 42,
        borderRadius: 21,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white"
    },
    avatar: {
        height: "100%",
        width: "100%",
        borderRadius: 160,
    },
    nameText: {
        fontSize: 24,
        color: Colors.white,
        fontFamily: "Montserrat-SemiBold",
    },
    emailText: {
        fontSize: 18,
        color: Colors.white,
        fontFamily: "Montserrat-Regular",
    },
    mobileText: {
        fontSize: 18,
        color: Colors.white,
        fontFamily: "Montserrat-Regular",
    }

})

export default TeacherUpdatePicture