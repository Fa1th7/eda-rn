import React, { FC, useEffect, useState } from 'react'
import { Incubator, Text, Colors, Button, View, LoaderScreen } from 'react-native-ui-lib';
import { observer } from 'mobx-react';
// import { SafeAreaView } from 'react-native-safe-area-context';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import { useStores } from '../../stores';

export const TeacherOnboard: FC = observer(() => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [monileNo, setMobileNo] = useState('');
  const {user, teacher} = useStores();
  useEffect(() => {
    if(user.user.name){
      setName(user.user.name);
    }
    if(user.user.email){
      setEmail(user.user.email);
    }
    if(user.user.mobileNo){
      setMobileNo(user.user.mobileNo);
    }
  },[user])

  const onSubmitHandler = () => {
    if(!user.user.teacher){
      teacher.createTeacher(name, monileNo, email);
      // teacher.createTeacher(name, monileNo, email);
    }
  }
  return (
    <SafeAreaView style = {styles.container}>
     { (teacher.isUpdaing || user.isUserUpdating || user.isUserFetching) ? <LoaderScreen  />: <ScrollView contentContainerStyle = {styles.scrollContentContainer} style = {styles.scorllContainer}>
        <View style = {styles.textGroup}>
      <Incubator.TextField
        // placeholder={'Name'}
        label={'Name'}
        labelStyle={styles.textInputLabel}
        containerStyle = {styles.containerTextInput}
        // floatingPlaceholder
        value = {name}
        floatingPlaceholderStyle={styles.floatingPlaceholder}
        style = {styles.textTextInput}
        onChangeText={(text) => setName(text)}
        enableErrors
        validate={['required', (value:string) => value.length > 6]}
        validationMessage={['Field is required', 'Email is invalid', 'Password is too short']}
        // showCharCounter
        maxLength={30}
      />
     {email? <Incubator.TextField
        // placeholder={'Name'}
        label={'Email'}
        labelStyle={styles.textInputLabel}
        containerStyle = {styles.containerTextInput}
        // floatingPlaceholder
        value = {email}
        editable = {false}
        floatingPlaceholderStyle={styles.floatingPlaceholder}
        style = {styles.textTextInput}
        onChangeText={(text) => setEmail(text)}
        enableErrors
        validate={['required', (value:string) => value.length > 6]}
        validationMessage={['Field is required', 'Email is invalid', 'Password is too short']}
        // showCharCounter
        maxLength={30}
      />:null}
      {monileNo?<Incubator.TextField
        // placeholder={'Name'}
        label={'Mobile No'}
        labelStyle={styles.textInputLabel}
        containerStyle = {styles.containerTextInput}
        editable = {false}
        // floatingPlaceholder
        value = {monileNo}
        floatingPlaceholderStyle={styles.floatingPlaceholder}
        style = {styles.textTextInput}
        onChangeText={(text) => setMobileNo(text)}
        enableErrors
        validate={['required', (value:string) => value.length > 6]}
        validationMessage={['Field is required', 'Email is invalid', 'Password is too short']}
        // showCharCounter
        maxLength={30}
      />:null}
      </View>
      <Button onPress={() => onSubmitHandler()} disabled = {!name && (!email || !monileNo) } style = {styles.buttonStyle} labelStyle = {styles.buttonTextStyle} label='Submit' size={Button.sizes.large} backgroundColor = {Colors.primary}/>
      </ScrollView>}
    </SafeAreaView>
  )
});

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scorllContainer:{
    padding:16,
    paddingTop:24,
   
  },
  scrollContentContainer:{
    height:"100%",
    // backgroundColor:"red",
    alignItems:"center",
    justifyContent:"space-between",
    width:"100%"
  },
  containerTextInput:{
    height: 40,
    // backgroundColor:'red',
    alignItems:"center",
    justifyContent:"center",
    borderBottomWidth:1,
    borderBottomColor:'#ccc',
    marginTop:32
  },
 textTextInput:{
    fontSize:18,
    color:Colors.textColor,
    fontFamily:"Montserrat-Regular",
 },
 floatingPlaceholder:{
  fontSize:18,
  color:"#ccc",
  fontFamily:"Montserrat-Medium",
 },
 textInputLabel:{
  fontSize:22,
  color:Colors.textColor,
  fontFamily:"Montserrat-Bold",
  alignSelf:"flex-start"
 },
 buttonStyle:{
   width:"100%",
  marginBottom:"5%",
  height:56
 },
 textGroup:{
    width:"100%",
    marginTop:"60%"
 },
 buttonTextStyle:{
    fontSize:18,
    color:Colors.textColor,
    fontFamily:"Montserrat-SemiBold",
 }
})
