import React, { FC } from 'react'
import { View, Text, Colors, TouchableOpacity } from 'react-native-ui-lib';
import { observer } from 'mobx-react';
import { SafeAreaView, StyleSheet, ActivityIndicator } from 'react-native';
import { getNavigationTheme } from '../../utils/designSystem';
import { Bounceable } from 'rn-bounceable';
import { useStores } from '../../stores/index';
import Loader from '../../components/loader';

export const SelectPreference: FC = observer(() => {
    const  {user} = useStores();
    const onPressHandler = (pref: string) => {
        user.updatePreference(pref);
    }
    return (

        <SafeAreaView style={styles.container}>
            {user.isUserUpdating?<Loader/>:<View style={styles.containerList}>
                <Bounceable onPress={ () => {
                    onPressHandler('teacher');
                }} contentContainerStyle={styles.buttonBackground} >
                    <Text style={styles.pref}>Teacher</Text>
                </Bounceable>
                <Bounceable onPress={() => onPressHandler('student')} contentContainerStyle={styles.buttonBackground} >
                    <Text style={styles.pref}>Student</Text>
                </Bounceable>
            </View>}
        </SafeAreaView>

    )
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.blackish,
    },
    containerList: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: "70%"
    },
    pref: {
        fontSize: 30,
        color: Colors.whitish,
        fontFamily: "Montserrat-Medium",

    },
    buttonBackground: {
        backgroundColor: Colors.blackish,
        width: "60%",
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 16,
        borderColor:Colors.secondary,
        borderWidth:0.2,
        shadowColor: "#FFF",
        shadowOffset: {
            width: 1,
            height: 1,

        },
        shadowOpacity: 0.30,
        shadowRadius: 1.41,

        elevation: 2,

    }
});
