import { ModalScreenLayouts, ScreenLayouts, TabScreenLayouts, AuthFlowS, OnboardFlows } from '../services/navigation/types';

import {Main} from './main';
import {Settings} from './settings';
import {Example} from './screen-sample';
import {genRootNavigator, genStackNavigator, genTabNavigator} from '../services/navigation/help';
import {services} from '../services';
import { observer } from 'mobx-react';
import {useStores} from '../stores';
import { Login } from './AuthFlow/Login';
import { View } from 'react-native-ui-lib';
import { FC } from 'react';
import { SelectPreference } from './OnboardFlow/selectPreference';
import { TeacherOnboard } from './OnboardFlow/teacherOnboard';
import { StudentOnboard } from './OnboardFlow/studentOnboard';
import { TeacherExpertiseUpdate } from './OnboardFlow/teacherExpertise';
import TeacherUpdatePicture from './OnboardFlow/teacherUpdatePicture';
import { TeacherLocationUpdate } from './OnboardFlow/teacherLocationUpdate';


// Describe your screens here
export type Tabs = 'Main' | 'WIP' | 'Settings';
export type Modal = 'ExampleModal';
export type Screen = 'Main' | 'Example' | 'Settings';
export type AuthFlow = 'Login' | 'Register' | 'ForgotPassword' | 'ResetPassword';
export type OnboardFlow = 'SelectPreference' | 'TeacherOnboard' | 'StudentOnboard' | 'TeacherUpdateExpertise' | 'TeacherPicturesUpdate' | 'TeacherLocationUpdate';

export type ModalProps = {
  ExampleModal: undefined;
};
export type ScreenProps = {
  Main: undefined;
  Example: ExampleScreenProps;
  Settings: undefined;
} & ModalProps;

const {t} = services;

// Screens
const screens: ScreenLayouts = {
  Main: {
    name: 'Main',
    component: Main,
    options: () => ({
      title: t.do('home.title'),
    }),
  },
  Example: {
    name: 'Example',
    component: Example,
    options: () => ({
      title: 'Example',
    }),
  },
  Settings: {
    name: 'Settings',
    component: Settings,
    options: () => ({
      title: 'Settings',
    }),
  },
};
const HomeStack = () => genStackNavigator([screens.Main, screens.Example]);
const ExampleStack = () => genStackNavigator([screens.Example]);
const SettingsStack = () => genStackNavigator([screens.Settings]);
const ExampleModalStack = () => genStackNavigator([screens.Main, screens.Example]);

// Tabs
const tabs: TabScreenLayouts = {
  Main: {
    name: 'MainNavigator',
    component: HomeStack,
    options: () => ({
      title: t.do('home.title'),
    }),
  },
  WIP: {
    name: 'ExampleNavigator',
    component: ExampleStack,
    options: () => ({
      title: 'WIP',
    }),
  },
  Settings: {
    name: 'SettingsNavigator',
    component: SettingsStack,
    options: () => ({
      title: 'Settings',
    }),
  },
};

const TabNavigator = () => genTabNavigator([tabs.Main, tabs.WIP, tabs.Settings]);
//OnboardFlow
const onboardFlow: OnboardFlows = {
  SelectPreference: {
    name: 'SelectPreference',
    component: SelectPreference,
    options: () => ({
      title: 'Select Preference',
    })
  },
  TeacherOnboard: {
    name: 'TeacherOnboard',
    component: TeacherOnboard,
    options: () => ({
      title: 'Teacher Onboard',
    })
  },
  TeacherUpdateExpertise:{
    name: 'TeacherUpdateExpertise',
    component: TeacherExpertiseUpdate,
    options: () => ({
      title: 'Update Expertise',
    })
  },
  TeacherPicturesUpdate:{
    name: 'TeacherPicturesUpdate',
    component: TeacherUpdatePicture,
    options: () => ({
      title: 'Upload Pictures',
    })
  },
  TeacherLocationUpdate:{
    name: 'TeacherLocationUpdate',
    component: TeacherLocationUpdate,
    options: () => ({
      title: 'Set location',
    })
  },
  StudentOnboard: {
    name: 'StudentOnboard',
    component: StudentOnboard,
    options: () => ({
      title: 'Student Onboard',
    })
  }
}
const OnBoardPrefFlow = () => genStackNavigator([onboardFlow.SelectPreference]);
const OnBoardTeacherOnboardFlow = () => genStackNavigator([onboardFlow.TeacherOnboard]);
const OnBoardStudentOnBoardFlow = () => genStackNavigator([onboardFlow.StudentOnboard]);
const OnBoardTeacherExpertiseFlow = () => genStackNavigator([onboardFlow.TeacherUpdateExpertise]);
const OnBoardTeacherPicturesUpdateFlow = () => genStackNavigator([onboardFlow.TeacherPicturesUpdate]);
const OnBoardTeacherFlow = () => genStackNavigator([onboardFlow.TeacherLocationUpdate]);
//AuthFlow

const authFlow: AuthFlowS = {
  Login: {
    name: 'Login',
    component: Login,
    options: () => ({
     headerShown: false,
    }),
  },
  Register: {
    name: 'Register',
    component: Main,
    options: () => ({
      title: t.do('Register.title'),
    }),
  },
  ForgotPassword: {
    name: 'ForgotPassword',
    component: Main,
    options: () => ({
      title: t.do('ForgotPassword.title'),
    }),
  },
  ResetPassword: {
    name: 'ResetPassword',
    component: Main,
    options: () => ({
      title: t.do('ResetPassword.title'),
    }),
  }
};
const AuthStack = () => genStackNavigator([authFlow.Login, authFlow.Register, authFlow.ForgotPassword, authFlow.ResetPassword]);

// Modals
const modals: ModalScreenLayouts = {
  ExampleModal: {
    name: 'ExampleModal',
    component: ExampleModalStack,
    options: () => ({
      title: 'ExampleModal',
    }),
  },
};

// Root Navigator
export const RootNavigator:FC = () => {
  
  
    return genRootNavigator(
      TabNavigator,
      AuthStack,
      OnBoardPrefFlow,
      OnBoardTeacherOnboardFlow,
      OnBoardStudentOnBoardFlow,
      OnBoardTeacherExpertiseFlow,
      OnBoardTeacherPicturesUpdateFlow,
      OnBoardTeacherFlow,
      [modals.ExampleModal]);
}

