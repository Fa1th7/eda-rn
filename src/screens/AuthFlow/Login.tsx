import React, { useCallback, useContext, useState } from 'react';
import { ScrollView, Alert, ActivityIndicator } from 'react-native';
import { useFocusEffect } from '@react-navigation/core';
import { View, Text, Colors, TouchableOpacity, Incubator } from 'react-native-ui-lib';
import { observer } from 'mobx-react';
import { If } from '@kanzitelli/if-component';
import Constants from 'expo-constants';
import * as Application from 'expo-application';

import { useServices } from '../../services';
import { useStores } from '../../stores';

import { Section } from '../../components/section';
import { Reanimated2 } from '../../components/reanimated2';
import { randomNum } from '../../utils/help';
import { BButton } from '../../components/button';
import FastImage from 'react-native-fast-image';
import PhoneInput from 'react-native-phone-number-input';
import { Bounceable } from 'rn-bounceable';
import { Action } from '../../components/action';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { TextInput } from 'react-native-gesture-handler';
import { AxiosContext } from '../../contexts/AxiosContext';
import { AuthContext } from '../../contexts/AuthContext';
import * as Keychain from 'react-native-keychain';
import API from '../../utils/Api';
type signInTypes = 'phone' | 'email';

export const Login: React.FC = observer(({ }) => {
    const { nav, t, api } = useServices();
    const { auth, user } = useStores();
    const [phone, setPhone] = useState('');
    const [signIntype, setSignInType] = useState<signInTypes>('phone');
    const [isSendOtploading, setisSendOtploading] = useState(false);
    const [isOtpSent, setIsOtpSent] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState("");
    const [isLoggingIn, setIsLogginIn] = useState(false);
    const [otp, setOtp] = useState("");
    const insets = useSafeAreaInsets();
    const [email, setEmail] = useState('');
    const { publicAxios } = useContext(AxiosContext);
    const authContext = useContext(AuthContext);

    const typeChangeHandler = (type: signInTypes) => {
        setSignInType(type);
    }
    const getCurrentSignType = (signIntypeProp: signInTypes) => {
        if (signIntype === signIntypeProp) {
            return true;
        }
        else {
            return false
        }
    }
    const onSendOtp = async () => {
        setisSendOtploading(true)
        if (signIntype === 'phone') {
            try {
                const { data } = await API.post('/auth/send_login_otp', {
                    mobileNo: phone
                });
                setisSendOtploading(false);
                setIsOtpSent(true);
                Alert.alert('Success', 'OTP sent successfully');
            } catch (e) {
                Alert.alert('Error', 'There was a problem fetching data :(');
                setisSendOtploading(false);
            }
        }
        else {
            try {
                const { data } = await API.post('/auth/send_login_otp', {
                    email: email
                });
                setisSendOtploading(false);
                setIsOtpSent(true);
                Alert.alert('Success', 'OTP sent successfully');
            } catch (e) {
                Alert.alert('Error', 'There was a problem fetching data :(');
                setisSendOtploading(false);
            }
        }
    }
    const onLogin = async () => {
        try {
            setIsLogginIn(true)
            if (signIntype === 'phone') {
                const response = await API.post('/auth/login_with_otp', {
                    mobileNo: phone,
                    code: otp,
                });

                const { token } = response.data?.data;
                auth.login({ accessToken: token });
                user.getUser();
                // const userData = await API.get('/api/v1/user/me');
                // const { data } = userData.data;
                // const {
                //     _id = '',
                //     name = '',
                //     email = '',
                //     lastPreference = null,
                //     mobileNo = null,
                //     teacher = null,
                //     student = null,
                //   } = data

                // user.setUser({_id, name, email, lastPreference, mobileNo, teacher, student});

                setIsLogginIn(false);
                // authContext.setAuthState({
                //     accessToken: token,
                //     authenticated: true,
                // });

                await Keychain.setGenericPassword(
                    'token',
                    JSON.stringify({
                        accessToken: token,
                    }),
                );

            }
            else {
                const response = await API.post('/auth/login_with_otp', {
                    email: email,
                    code: otp,
                });

                const { token, refreshToken } = response.data.data;
                auth.login({ accessToken: token });
                setIsLogginIn(false);

                await Keychain.setGenericPassword(
                    'token',
                    JSON.stringify({
                        accessToken: token,
                    }),
                );

            }

        } catch (error) {
            setIsLogginIn(false);
            Alert.alert('Login Failed');
            console.log(error)
        }
    };



    return (
        <FastImage style={{ height: "100%", width: "100%", flex: 1 }} source={require("../../assets/images/loginDarkBg.jpeg")}>
            <ScrollView keyboardDismissMode='interactive' scrollEnabled={true} contentContainerStyle={{ alignItems: "center", justifyContent: "flex-start", flex: 1, paddingTop: insets.top, }}>
                <View style={{ width: "100%", alignItems: "center", justifyContent: "center", marginTop: 64 }}>
                    <Text logoText style={{ color: Colors.whitish, }}>EDA</Text>
                    <Text h3 style={{ color: Colors.whitish, fontFamily: "Montserrat-Regular" }}>Learn, Grow, Conquer</Text>
                </View>
                {/* <If _={getCurrentSignType('phone')} _then={<PhoneVersion />} _else={<EmailVersion />} /> */}
                {
                    isOtpSent ? <View style={{ width: 300, height: 184, alignSelf: "center", borderRadius: 8, borderWidth: 0.5, borderColor: Colors.textColor, justifyContent: "flex-start", paddingTop: 10, paddingBottom: 10, marginTop: 152, }}>
                        <Incubator.TextField
                            placeholder={`Enter OTP`}
                            //floatingPlaceholder
                            value={otp}
                            onChangeText={(text) => {

                                setOtp(text);
                            }}
                            enableErrors
                            //fieldStyle = {{}}
                            // validateOnBlur
                            //validateOnChange
                            fieldStyle={{ height: "100%" }}
                            keyboardType='url'
                            floatOnFocus
                            style={{ color: Colors.textColor, fontSize: 18, fontFamily: 'Montserrat-Regular', marginTop: 0 }}
                            placeholderTextColor={Colors.textColor}
                            containerStyle={{ borderBottomColor: Colors.textColor, borderBottomWidth: 0.5, width: "80%", height: 40, alignSelf: "center", marginBottom: 16 }}
                        // validate={['required', 'email', (value: String) => value.length > 6]}
                        // validationMessage={['Email is required', 'Email is invalid', 'Password is too short']}

                        />

                        <Bounceable onPress={() => onLogin()}>
                            <View style={{ backgroundColor: Colors.primary, height: 40, width: "80%", alignSelf: "center", justifyContent: "center", alignItems: "center", borderRadius: 8, marginTop: 26 }}>
                                {isLoggingIn ? <ActivityIndicator size={'small'} color='white' /> : <Text buttonText whitish>Login</Text>}
                            </View>
                        </Bounceable>
                    </View> : <View>
                        {signIntype === ('phone') ? <View style={{ width: 300, height: 184, alignSelf: "center", borderRadius: 8, borderWidth: 0.5, borderColor: Colors.textColor, justifyContent: "flex-start", paddingBottom: 10, paddingTop: 10, marginTop: 152, }} >
                            <PhoneInput
                                //defaultValue={phone}
                                value={phoneNumber}
                                defaultCode="IN"
                                layout="first"
                                onChangeText={(text) => {
                                    setPhoneNumber(text);
                                }}
                                onChangeFormattedText={(text) => {
                                    setPhone(text);
                                }}
                                containerStyle={{ backgroundColor: "transparent", width: "80%", alignSelf: "center", borderBottomWidth: 0.5, borderBottomColor: Colors.textColor, marginBottom: 16, }}
                                withDarkTheme={true}
                                //countryPickerButtonStyle = {}
                                // flagButtonStyle = {{backgroundColor:0}}
                                textInputStyle={{ color: Colors.textColor, fontSize: 18, fontFamily: 'Montserrat-Regular', }}
                                textContainerStyle={{ backgroundColor: "transparent", paddingLeft: 0, marginTop: 0, paddingTop: 10, paddingBottom: 10 }}
                                codeTextStyle={{ color: "white", fontSize: 18, fontFamily: 'Montserrat-Regular', marginRight: 0 }}
                                autoFocus
                            />
                            <Bounceable onPress={() => onSendOtp()}>
                                <View style={{ backgroundColor: Colors.primary, height: 40, width: "80%", alignSelf: "center", justifyContent: "center", alignItems: "center", borderRadius: 8, marginTop: 24 }}>
                                    {isSendOtploading ? <ActivityIndicator size={'small'} color='white' /> : <Text buttonText whitish>Send OTP</Text>}
                                </View>
                            </Bounceable>
                            <TouchableOpacity onPress={() => typeChangeHandler('email')} style={{ marginTop: 14, alignItems: "center" }}>
                                <Text white style={{ fontSize: 14, fontFamily: 'Montserrat-Regular', color: Colors.secondary }}>Sign in via email</Text>
                            </TouchableOpacity>
                        </View> :
                            <View style={{ width: 300, height: 184, alignSelf: "center", borderRadius: 8, borderWidth: 0.5, borderColor: Colors.textColor, justifyContent: "flex-start", paddingTop: 10, paddingBottom: 10, marginTop: 152, }} >
                                <Incubator.TextField
                                    placeholder={'Email'}
                                    //floatingPlaceholder
                                    value={email}
                                    onChangeText={(text) => {

                                        setEmail(text);
                                    }}
                                    enableErrors
                                    //fieldStyle = {{}}
                                    // validateOnBlur
                                    //validateOnChange
                                    fieldStyle={{ height: "100%" }}
                                    keyboardType='url'
                                    floatOnFocus
                                    style={{ color: Colors.textColor, fontSize: 18, fontFamily: 'Montserrat-Regular', marginTop: 0 }}
                                    placeholderTextColor={Colors.textColor}
                                    containerStyle={{ borderBottomColor: Colors.textColor, borderBottomWidth: 0.5, width: "80%", height: 40, alignSelf: "center", marginBottom: 16 }}
                                // validate={['required', 'email', (value: String) => value.length > 6]}
                                // validationMessage={['Email is required', 'Email is invalid', 'Password is too short']}

                                />

                                <Bounceable onPress={() => onSendOtp()}>
                                    <View style={{ backgroundColor: Colors.primary, height: 40, width: "80%", alignSelf: "center", justifyContent: "center", alignItems: "center", borderRadius: 8, marginTop: 26 }}>
                                        {isSendOtploading ? <ActivityIndicator size={'small'} color='white' /> : <Text buttonText whitish>Send OTP</Text>}
                                    </View>
                                </Bounceable>
                                <TouchableOpacity onPress={() => typeChangeHandler('phone')} style={{ marginTop: 14, alignItems: "center" }}>
                                    <Text white style={{ fontSize: 14, fontFamily: 'Montserrat-Regular', color: Colors.secondary }}>Sign in via phone number</Text>
                                </TouchableOpacity>
                            </View>

                        }
                    </View>
                }


            </ScrollView>
        </FastImage>
    );
});
