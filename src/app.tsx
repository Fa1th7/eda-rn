import React, { FC } from 'react';
import { StatusBar, useColorScheme, Platform } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import {RootNavigator} from './screens';
import {getNavigationTheme, getThemeStatusBarStyle} from './utils/designSystem';
import {useServices} from './services';
import { QueryClient, QueryClientProvider, focusManager } from 'react-query';
import { useOnlineManager } from './hooks/useOnlineManager';
import { useAppState } from './hooks/useAppState';

function onAppStateChange(status:string) {
  // React Query already supports in web browser refetch on window focus by default
  if (Platform.OS !== 'web') {
    focusManager.setFocused(status === 'active');
  }
}

const queryClient = new QueryClient({
  defaultOptions: { queries: { retry: 2 } },
});

export const AppNavigator: FC = () => {
  useColorScheme();
  useOnlineManager();

  useAppState(onAppStateChange);
  const {nav} = useServices();

  return (
    <>
      <StatusBar barStyle={getThemeStatusBarStyle()} />
      <QueryClientProvider client={queryClient}>
      <NavigationContainer
        ref={nav.n}
        onReady={nav.onReady}
        onStateChange={nav.onStateChange}
        theme={getNavigationTheme()}
      >
        <RootNavigator />
      </NavigationContainer>
      </QueryClientProvider>
    </>
  );
};
