import { useEffect } from 'react';
import { AppState } from 'react-native';

export function useAppState(onChange: (state: string) => void) {
    console.log(onChange);
  useEffect(() => {
   let listener = AppState.addEventListener('change', onChange);
    return () => {
    //   AppState.removeEventListener('change', onChange);
    listener.remove()
    };
  }, [onChange]);
}
