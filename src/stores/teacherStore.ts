import {makeAutoObservable} from 'mobx';
import {hydrateStore, makePersistable} from 'mobx-persist-store';
import Snackbar from 'react-native-snackbar';
import API from '../utils/Api';
import { stores } from './index';

type profilepicType = {
  url: string,
  name: string,
  path: string,
  public: boolean,
  size: number,
  type: string,
} | null

type dataToUpdateType = {
  board: Array<string>;
  expertise: Array<string>;
  onBoardStep: number;
  afterUpdate: () => void;
}
type locationType = {
  latitude: number;
  longitude: number;
}

export class TeacherStore implements IStore {
  name = "";
  mobileNo = "";
  email = "";
  isUpdaing = false;
  onBoardStep = 0;
  isFeatured = false;
  isVerified = false;
  _id = "";
  board = [];
  expertise = [];
  location = null;
  profilePic = null
  constructor() {
    makeAutoObservable(this);

    makePersistable(this, {
      name: TeacherStore.name,
      properties: ['onBoardStep'],
    });
  }
  setTeacher = (teacher: any): void => {
    this.name = teacher.name;
    this.mobileNo = teacher.mobileNo;
    this.email = teacher.email;
    this.isFeatured = teacher.isFeatured;
    this.isVerified = teacher.isVerified;
    this._id = teacher._id;
    this.board = teacher.board;
    this.expertise = teacher.expertise;
    this.onBoardStep = teacher.onBoardStep;
    this.profilePic = teacher.profilePic;
    this.location = teacher.location;
  }
  setIsUpdating = (v: boolean): void => {
    this.isUpdaing = v;
  }
  updateTeacher = async (id:string,dataToUpdate: dataToUpdateType): PVoid => {
    try {
      this.setIsUpdating(true);
      let data = await API.put(`/api/v1/teacher/update/${id}`, {
       ...dataToUpdate
      })
      stores.user.getUser();
      this.setIsUpdating(false);
      dataToUpdate.afterUpdate();
    } catch (error) {
      console.log(error, 'user update error')
      this.setIsUpdating(false);
      Snackbar.show({
        text: 'Could not update teacher expertise',
        textColor: '#fc03ca',
        backgroundColor: '#383b42',
        duration: Snackbar.LENGTH_LONG,
       
    })
    }
  
  }
  updateTeacherProfilePic = async (id:string,dataToUpdate: profilepicType, onBoardStep: number ): PVoid => {
    try {
      this.setIsUpdating(true);
      let data = await API.put(`/api/v1/teacher/update/${id}`, {
       profilePic: dataToUpdate,
       onBoardStep: onBoardStep
      })
      console.log(data)
      stores.user.getUser();
      this.setIsUpdating(false);
    } catch (error) {
      console.log(error, 'user update error')
      this.setIsUpdating(false);
      Snackbar.show({
        text: 'Could not update profile picture',
        textColor: '#fc03ca',
        backgroundColor: '#383b42',
        duration: Snackbar.LENGTH_LONG,
       
    })
    }
  
  }
  updateTeacherLocation = async (id:string,dataToUpdate: locationType, onBoardStep: number ): PVoid => {
    try {
      this.setIsUpdating(true);
      let data = await API.put(`/api/v1/teacher/update/${id}`, {
       location: {
        type: 'Point',
        coordinates: [dataToUpdate.longitude, dataToUpdate.latitude]
       },
       onBoardStep: onBoardStep
      })
      console.log(data)
      stores.user.getUser();
      this.setIsUpdating(false);
    } catch (error) {
      console.log(error, 'user update error')
      this.setIsUpdating(false);
      Snackbar.show({
        text: 'Could not update user location',
        textColor: '#fc03ca',
        backgroundColor: '#383b42',
        duration: Snackbar.LENGTH_LONG,
       
    })
    }
  
  }
  createTeacher =  async (name:string, mobileNo: string, email:string): PVoid => {
      try {
       this.setIsUpdating(true);
        let res =  await API.post('/api/v1/teacher/create', {
            name, email, mobileNo, userId: stores.user.user._id
        });
        if(stores.user.user.name === ""){
            const userData = await API.put('/api/v1/user/update-profile', {name: name});
        }
        stores.user.getUser();
        this.setIsUpdating(false);
      } catch (error) {
          console.log(error)
      }
     
  }

  hydrate = async (): PVoid => {
    await hydrateStore(this);
  };
}
