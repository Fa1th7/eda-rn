import {makeAutoObservable} from 'mobx';
import {hydrateStore, makePersistable, clearPersistedStore} from 'mobx-persist-store';
import * as Keychain from 'react-native-keychain';

type loginCredentials = {accessToken: string};
export class AuthStore implements IStore {
  isLoggedIn = false;
  isLoggingIn = false;
  accessToken = "";
  refreshToken = "";

  
  
  constructor() {
    makeAutoObservable(this);

    // makePersistable(this, {
    //   name: AuthStore.name,
    //   properties: ['isLoggedIn'],
    // });
  }
  async clearStoredDate() {
    await clearPersistedStore(this);
  }
  login = ({accessToken}:loginCredentials): void => {
    this.isLoggedIn = true;
    this.accessToken = accessToken;
  };
  
  logout = async ():Promise <void> => {
    await Keychain.resetGenericPassword();
    this.isLoggedIn = false;
    this.accessToken = "";
  };

  setIsLoggingIn = (v: boolean): void => {
    this.isLoggingIn = v;
  };


  hydrate = async (): PVoid => {
    await hydrateStore(this);
  };
}
