import {makeAutoObservable} from 'mobx';
import {hydrateStore, makePersistable} from 'mobx-persist-store';
import API from '../utils/Api';
import { stores } from './index';

type user = {
    _id: string;
    name: string;
    email: string;
    lastPreference: string | null;
    mobileNo: string | null;
    teacher: object | null;
    student: object | null;
}
export class UserStore implements IStore {
  user: user = {
    _id: "",
    name: "",
    email: "",
    lastPreference: null,
    mobileNo: null,
    student: null,
    teacher: null,

  };
  isUserFetching = false;
  isUserUpdating = false;



  constructor() {
    makeAutoObservable(this);

    makePersistable(this, {
      name: UserStore.name,
      properties: ['user'],
    });
  }
  setUser = (user: user): void => {
    this.user = user;
  }
  setIsFetching = (v: boolean): void => {
    this.isUserFetching = v;
  }
  getUser = async (): Promise<void> => {
    this.setIsFetching(true);
    const userData = await API.get('/api/v1/user/me');
    const { data } = userData.data;
    const {
        _id = '',
        name = '',
        email = '',
        lastPreference = null,
        mobileNo = null,
        teacherData = null,
        studentData = null,
      } = data
      if(teacherData){
        stores.teacher.setTeacher(teacherData);
      }
      this.setUser({_id, name, email, lastPreference, mobileNo,  teacher: teacherData, student: studentData});
      this.setIsFetching(false);

  };
  setUserUpdating = (v: boolean): void => {
    this.isUserUpdating = v;
  }
  updatePreference = async (preference: string): Promise<void> => {
    this.setUserUpdating(true);
    const userData = await API.put('/api/v1/user/update-profile', {lastPreference: preference});
    const { data } = userData.data;
    const {
        _id = '',
        name = '',
        email = '',
        lastPreference = null,
        mobileNo = null,
        teacherData = null,
        studentData = null,
      } = data;
    this.setUser({_id, name, email, lastPreference, mobileNo, teacher: teacherData, student: studentData});
    this.setUserUpdating(false);

  }

  hydrate = async (): PVoid => {
    await hydrateStore(this);
  };
}
