import React, { FC, useContext } from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import merge from 'lodash/merge';

import {screenDefaultOptions, tabBarDefaultOptions} from './options';
import {GenStackNavigatorProps, GenTabNavigatorProps, ModalScreenInfo} from './types';
import {useColorScheme} from 'react-native';
import { observer } from 'mobx-react';
import { useStores } from '../../stores';
import { AuthContext } from '../../contexts/AuthContext';
import { OnboardFlow } from '../../screens/index';
import { TeacherExpertiseUpdate } from '../../screens/OnboardFlow/teacherExpertise';
import { TeacherLocationUpdate } from '../../screens/OnboardFlow/teacherLocationUpdate';

export const genStackNavigator = (screens: GenStackNavigatorProps): JSX.Element => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useColorScheme(); // needs to be here to correctly change nav bar appearance

  const Stack = createNativeStackNavigator();
  const stackScreens = screens.map(it => (
    <Stack.Screen
      key={it.name}
      name={it.name}
      component={it.component}
      options={merge(screenDefaultOptions(), it.options())}
    />
  ));

  return <Stack.Navigator>{stackScreens}</Stack.Navigator>;
};

export const genTabNavigator = (screens: GenTabNavigatorProps): JSX.Element => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useColorScheme(); // needs to be here to correctly change tab bar appearance

  const Tab = createBottomTabNavigator();
  const tabScreens = screens.map(it => (
    <Tab.Screen key={it.name} name={it.name} component={it.component} options={it.options()} />
  ));

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        ...tabBarDefaultOptions(route.name),
      })}
    >
      {tabScreens}
    </Tab.Navigator>
  );
};

export const genRootNavigator = (app: React.FC, authComp: React.FC, onBoardPrefFlow:React.FC, onBoardTeacherFlow: React.FC, onBoardStudentFlow: React.FC, teacherExpertise: React.FC, TeacherPicturesUpdate:React.FC, OnBoardTeacherFlow:React.FC, modals: ModalScreenInfo[]): JSX.Element => {
  const RootStack = createNativeStackNavigator();
  const appScreen = <RootStack.Screen name="App" component={app} />;
  const OnboarPrefdFlowScreen = <RootStack.Screen name="OnboardFlow" component={onBoardPrefFlow} />;
  const OnboarTeacherFlowScreen = <RootStack.Screen name="OnboardFlow" component={onBoardTeacherFlow} />;
  const OnboarStudentFlowScreen = <RootStack.Screen name="OnboardFlow" component={onBoardStudentFlow} />;
  const TeacherExpertiseScreen = <RootStack.Screen name="TeacherExpertise" component={teacherExpertise} />;
  const TeacherPicturesUpdateScreen = <RootStack.Screen name="TeacherPicturesUpdateOnboard" component={TeacherPicturesUpdate} />;
  const OnBoardTeacherFlowScreen = <RootStack.Screen name="OnBoardTeacherFlow" component={OnBoardTeacherFlow} />;
  const modalScreens = modals.map(m => <RootStack.Screen key={m.name} name={m.name} component={m.component} />);
  
  const Nav : FC = observer(({}) => {
    const {auth, user, teacher} = useStores();

    return (
      <RootStack.Navigator screenOptions={{headerShown: false}}>
      <>
      {
        
        auth.isLoggedIn ?
        <>
        {
          (!user.user.lastPreference && !user.user.teacher && !user.user.student)?<RootStack.Group>{OnboarPrefdFlowScreen}</RootStack.Group>:((user.user.lastPreference === 'teacher') && !user.user.teacher )  ?<RootStack.Group>{OnboarTeacherFlowScreen}</RootStack.Group>:(user.user.lastPreference === 'student' && !user.user.student)?<RootStack.Group>{OnboarStudentFlowScreen}</RootStack.Group>
          :(user.user.lastPreference === 'teacher' && user.user.teacher && teacher.onBoardStep === 1)?<RootStack.Group>{TeacherExpertiseScreen}</RootStack.Group> : (user.user.lastPreference === 'teacher' && user.user.teacher && teacher.onBoardStep === 2)?<RootStack.Group>{TeacherPicturesUpdateScreen}</RootStack.Group>:(user.user.lastPreference === 'teacher' && user.user.teacher && teacher.onBoardStep === 3)?<RootStack.Group>{OnBoardTeacherFlowScreen}</RootStack.Group>: <RootStack.Group>{appScreen}</RootStack.Group>
        }
        
        {/* <RootStack.Group>{appScreen}</RootStack.Group>  */}
        <RootStack.Group screenOptions={{presentation: 'modal'}}>{modalScreens}</RootStack.Group>
        </>
        :<RootStack.Screen name="Auth" component={authComp} />
      }
      

     
      </>
      </RootStack.Navigator>
    )

    
  })
  return (
   
     <Nav />
 
  );
};
