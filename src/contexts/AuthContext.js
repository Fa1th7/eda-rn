import React, {createContext, useEffect, useState} from 'react';
import * as Keychain from 'react-native-keychain';
import { useStores } from '../stores';

const AuthContext = createContext(null);
const {Provider} = AuthContext;

const AuthProvider = ({children}) => {
  const [authState, setAuthState] = useState({
    accessToken: null,
    refreshToken: null,
    authenticated: null,
  });
  const {auth} = useStores();

  // useEffect(() => {
  //   if(authState.authenticated){
  //     auth.login()
  //   }
  // },[authState.authenticated])

  const logout = async () => {
    await Keychain.resetGenericPassword();
    setAuthState({
      accessToken: null,
      refreshToken: null,
      authenticated: false,
    });
    auth.isLoggedIn = false;
  };
  

  const getAccessToken = () => {
    return authState.accessToken;
  };

  return (
    <Provider
      value={{
        authState,
        getAccessToken,
        setAuthState,
        logout,
      }}>
      {children}
    </Provider>
  );
};

export {AuthContext, AuthProvider};